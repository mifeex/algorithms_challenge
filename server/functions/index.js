const functions = require("firebase-functions");

const admin = require('firebase-admin');
admin.initializeApp();

const cors = require('cors')({ origin: true });
const axios = require('axios');

// this function works fine when called the exact same way as the one below
exports.helloWorld = functions.https.onRequest((request, response) => {
    console.log("Hello World");
});

exports.runCode = functions.https.onRequest((request, response) => {
    console.log(request.body)
    cors(request, response, () => {
        const data = request.body;
        axios.post("https://glot.io/api/run/python/latest",data, {
            headers: {
                'Authorization': 'Token 2a441d77-bd28-4551-a7c0-0120a076cab5',
                'Content-type': 'application/json'
            }
        })
            .then(r => {
                // console.log("Cool", r);s
                response.json(r.data);
            })
            .catch(e => {
                console.log("¯\_(ツ)_/¯ error: ", e);
                response.sendStatus(500);
            })
    })
})

exports.searchActivities = functions.https.onRequest((request, response) => {
    // enable cors 
    cors(request, response, () => {
        // request to yelp api
        axios.get("https://glot.io/api/snippets?page=5&per_page=3",)
            .then(r => {
                console.log("Cool", r.data);
                response.json(r.data);
            })
            .catch(e => {
                console.log("¯\_(ツ)_/¯ error: ", e);
                response.sendStatus(500);
            })
    })

})

exports.makeUppercase = functions.firestore.document('/messages/{documentId}')
    .onCreate((snap, context) => {
        // Grab the current value of what was written to Firestore.
        const original = snap.data().original;

        // Access the parameter `{documentId}` with `context.params`
        functions.logger.log('Uppercasing', context.params.documentId, original);

        const uppercase = original.toUpperCase();

        // You must return a Promise when performing asynchronous tasks inside a Functions such as
        // writing to Firestore.
        // Setting an 'uppercase' field in Firestore document returns a Promise.
        return snap.ref.set({ uppercase }, { merge: true });
    });

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
