import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import userReducer from "./reducers/user-reducer";
import type {userInfo} from "./reducers/user-reducer"

export interface Store {
    users: userInfo;
}

const reducers = combineReducers({
    users: userReducer
});

const store = createStore(reducers, applyMiddleware(thunk));
declare global {
    interface Window { store: any; }
}

window.store = store || {};
// console.log(store)
export default store;