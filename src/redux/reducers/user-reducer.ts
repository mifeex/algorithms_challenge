import type {ThunkDispatch} from "redux-thunk"
import { completeSighIn, addNewUserToDB} from "../../api/firestore/API_SKD/getAPI"
import type {userAuthType} from "../../api/firestore/API_SKD/getAPI"
import {onAuthStateChanged, signInWithEmailAndPassword, signOut, sendSignInLinkToEmail} from "firebase/auth"
import {auth, actionCodeSettings} from "../../api/firestore/instance"
import Base64 from 'crypto-js/enc-base64';
import hmacSHA512 from 'crypto-js/hmac-sha512';
// const API = new GetApi()

export interface regUser {
    id?: number;
    email: string;
    name: string;
    password: string;
    validPassword?: string;
    isVIP: boolean;
    isPassValid: boolean | null;
    isChecked?: boolean;
}

export interface LoginUser {
    email: string;
    password: string;
    isVIP: boolean;
}

export interface userInfo {
    registerCase?: regUser | any;
    loginCase?: LoginUser | any;
    checked: boolean;
    isUserAuth: userAuthType;
    error: string;
    isAuth: boolean;
}

export const initialState: userInfo = {
    registerCase: {
        email: "",
        name: "",
        password: "",
        validPassword: "",
        isVIP: false,
        isPassValid: null
    } as regUser, 
    loginCase: {
        email: "",
        password: "",
        isVIP: false
    } as LoginUser, 
    checked: false,
    isUserAuth: {
        email: "",
        uid: "",
        isAuth: false,
    },
    error: "",
    isAuth: false
}

const userReducer = (state: typeof initialState = initialState, action: any): typeof initialState => {
    let newState = {...state}
    let regCase = {...newState.registerCase}
    let logCase = {...newState.loginCase}

    const _validateEmail = (email: string, isRegister: boolean) => {
        if(isRegister) {
            return newState.registerCase.email = email
        }

        return newState.loginCase.email = email;
    }

    const _validateUsername = (username: string) => {
        return newState.registerCase.name = username
    }
    
    const _savePassword = (password: string, isRegister: boolean) => {
        if(isRegister) {
            if(password.length < 8) newState.error = "Password should be at least 8 characters";
            return newState.registerCase.password = password
        }

        return newState.loginCase.password = password;
    }

    const _validatePassword = (newPassword: string) => {
        newState.registerCase.validPassword = newPassword;
        if(newPassword === newState.registerCase.password) {
            return newState.registerCase.isPassValid = true;
        };

        return newState.registerCase.isPassValid = false;
    }

    const _saveUser = (checked: boolean) => newState.checked = checked;

    const _setUserAuth = (authData: userAuthType) => {
        newState.isUserAuth.email = authData.email
        newState.isUserAuth.uid= authData.uid
        newState.isUserAuth.isAuth = authData.isAuth
        newState.isAuth = true
    }

    const _logout = () => {
        newState.isUserAuth.email = ""
        newState.isUserAuth.uid= ""
        newState.isUserAuth.isAuth = false
        newState.isAuth = false
    }

    const _regCleaner = (login: boolean) => {
        const clearLogon = () => {
            regCase.username = "";
            regCase.validPassword = "";
            regCase.password = "";
            regCase.email = "";
            regCase.isVIP = false;
            regCase.isPassValid = false;
        }
        const clearLogin = () => {
            logCase.email = "";
            logCase.password = "";
            logCase.isVIP = false;
        }
        newState.isAuth = false;
        login ? clearLogon() : clearLogin();
    }
    
    switch(action.type) {
        case "ADD_NEW_USERS_IN_LIST":     
            return newState;
        case "SAVE_PASSWORD_IN_MEMORY":
            _savePassword(action.password.password, action.password.isRegister);
            return newState;
        case "VALIDATE_PASSWORD_IN_MEMORY":
            _validatePassword(action.validatedPassword);
            return newState;
        case "SAVE_EMAIL":
            _validateEmail(action.validEmail.email, action.validEmail.isRegister)
            return newState;
        case "SAVE_USERNAME":
            _validateUsername(action.username);
            return newState;
        case "SAVE_USER_IN_MEMORY":
            _saveUser(action.isSaved);
            return newState;
        case "SET_USER_AUTH":
            _setUserAuth(action.user)
            return newState;
        case "LOGOUT":
            _logout()
            return newState;
        case "KILL_VALUE":
            _regCleaner(action.login);
            return newState;
        default:
            return state
    }
}

const clearValues = (login : boolean) => ({type: "KILL_VALUE", login})

const registerActionCreator = (email: string) => ({type: "SET_USER_EMAIL_INVALID", email})
export const logUsers = (value: regUser) => (dispatch: any) => {
    // sendSignInLinkToEmail(auth, value.email, actionCodeSettings("code?=12"))
    // .then(() => {
        // The link was successfully sent. Inform the user.
        // Save the email locally so you don't need to ask the user for it again
        // if they open the link on the same device.
        window.localStorage.setItem('emailForSignIn', value.email);
        addNewUserToDB(value)
        // ...
    // })
    // .catch((error) => {
    //     const errorCode = error.code;
    //     const errorMessage = error.message;
    //     console.log(errorMessage);
    //     // ...
    // });
    dispatch(registerActionCreator(value.email))
    dispatch(clearValues(true))
}

const setUserEmailValidActionCreator = () => ({type: "SET_USER_EMAIL_VALID"})
export const finishRegistration = () => (dispatch: any) => {
    completeSighIn(window.localStorage.getItem('emailForSignIn'), window.localStorage.getItem('tokenForSignIn'));
    dispatch(setUserEmailValidActionCreator())
}

const saveEmailActionCreator = (email: string, isRegister: boolean) => ({type: "SAVE_EMAIL", validEmail: {email, isRegister}})
export const saveEmail = (email: string, isRegister: boolean) => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    dispatch(saveEmailActionCreator(email, isRegister))
}

const saveUsernameActionCreator = (username: string) => ({type: "SAVE_USERNAME", username})
export const saveUsername = (username: string) => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    dispatch(saveUsernameActionCreator(username))
}

const savePasswordActionCreator = (password: string, isRegister: boolean) => ({type: "SAVE_PASSWORD_IN_MEMORY", password: {password, isRegister}})
export const savePassword = (password: string, isRegister: boolean) => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    dispatch(savePasswordActionCreator(password, isRegister))
}

const validatePasswordActionCreator = (validatedPassword: string) => ({type: "VALIDATE_PASSWORD_IN_MEMORY", validatedPassword})
export const validatePassword = (validatedPassword: string) => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    dispatch(validatePasswordActionCreator(validatedPassword))
}

const rememberUsernameActionCreator = (isSaved: boolean) => ({type: "SAVE_USER_IN_MEMORY", isSaved})
export const rememberUsername = (isSaved: boolean) => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    dispatch(rememberUsernameActionCreator(isSaved))
}

const checkUserAuthActionCreator = (user: userAuthType) => ({type: "SET_USER_AUTH", user});
export const checkUserAuth = (user: userAuthType) => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    onAuthStateChanged(auth, (user) => {
        if (user) {
            const uid = user.uid;
            const email = user.email;
            dispatch(checkUserAuthActionCreator(({email, uid, isAuth: true})));
        } else {
            console.log("auth")
        }
    })
}

const loginUserActionCreator = (user: LoginUser) => ({type: "LOGIN_USER", user});
export const loginUser = (user: LoginUser) => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    
    signInWithEmailAndPassword(auth, user.email, user.password)
    .then((userCredential) => {
      // Signed in 
      const user = userCredential.user;
      dispatch(clearValues(false))
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      console.log(error.message);
    });
}

const logoutActionCreator = () => ({type: "LOGOUT" })
export const logout = () => (dispatch: ThunkDispatch<userInfo, void, any>) => {
    signOut(auth).then(() => {
        // Sign-out successful.
        dispatch(logoutActionCreator())
      }).catch((error) => {
        // An error happened.
      });
}

export default userReducer