import React from "react";
import { Form, Button, FloatingLabel, Alert } from "react-bootstrap";
import s from "./style.module.css"
import { useHistory } from 'react-router-dom';

const FormController: React.FC<any> = (props) => {
    return (
        <>
            <FloatingLabel
                controlId="floatingInput"
                label="Email address"
                className="mb-3"
            >
                <Form.Control
                    type="email"
                    placeholder="name@example.com"
                    onChange={e => props.validateEmail(e.target.value)}
                />
            </FloatingLabel>
            <FloatingLabel
                controlId="floatingPassword"
                label="Password"
                className="mb-3"
            >
                <Form.Control
                    type="password"
                    placeholder="Password"
                    onChange={(e) => {
                        props.savePassword(e.target.value);
                    }}
                />
            </FloatingLabel>
        </>
    )
};

const Login: React.FC<any> = (props: any) => {
    const Log = props.regUser ? "Register" : "Login";
    const history = useHistory()

    const validateEmail = (email: string) => {
        props.regUser ? props.saveEmail(email, true) : props.saveEmail(email, false);
    };

    const savePassword = (password: string) => {
        props.regUser ? props.savePassword(password, true) : props.savePassword(password, false);
    };

    const validatePassword = (password: string) => props.validatePassword(password);

    const validateUsername = (username: string) => props.saveUsername(username);

    const rememberUser = (checked: boolean) => props.rememberUsername(checked);

    const submitUserInputs = (e:any) => {
        // debugger
        const value = {
            email: props.registerCase.email,
            name: props.registerCase.name,
            password: props.registerCase.password,
            isPassValid: props.registerCase.isPassValid,
            isVIP: false,
            isChecked: props.checked
        } as any;
        e.preventDefault();
        props.logUsers(value);
        history.push("/");
    }

    const authUser = (e:any) => {
        const value = {
            email: props.loginCase.email,
            password: props.loginCase.password,
            isVIP: false
        } as any;
        e.preventDefault();
        props.loginUser(value);
        history.push("/");
    }

    return (
        <Form onSubmit={e => props.regUser ? submitUserInputs(e) : authUser(e)}>
        <Alert.Heading>{Log}</Alert.Heading>
        {props.regUser ?
            <>
                <FloatingLabel
                    controlId="floatingUserName"
                    label="Username"
                    className="mb-3"
                >
                    <Form.Control
                        type="username"
                        placeholder="UserName"
                        onChange={e => validateUsername(e.target.value)}
                        value={props.username}
                    />
                </FloatingLabel>

                <FormController
                    validateEmail={validateEmail}
                    savePassword={savePassword}
                    password={props.password}
                    email={props.email}
                />

                <FloatingLabel
                    controlId="floatingConfirmPassword"
                    label="Confirm Password"
                    className="mb-3"
                >
                    <Form.Control
                        // className={props.error !== "" ? s.inValidValue : ""}
                        type="password"
                        placeholder="Confirm Password"
                        onChange={(e) => validatePassword(e.target.value)}
                        value={props.validPassword}
                    />
                </FloatingLabel >
            </> :
            <>
                <FormController
                    validateEmail={validateEmail}
                    savePassword={savePassword}
                    password={props.password}
                    email={props.email}
                />
            </>
        }
        <Form.Group
            className="mb-3"
            controlId="formBasicCheckbox"
        >
            <Form.Check
                type="checkbox"
                label="Check me out"
                onChange={e => rememberUser(e.target.checked)}
                value={props.checked}
            />
        </Form.Group>
        <Button variant="primary" type="submit">
            {Log}
        </Button>
    </Form>
    )
}

export default Login;