import React from "react";
import Log from "./Log";
import {connect} from "react-redux";
import type {Store} from "../../redux/store"
import {savePassword, saveEmail, rememberUsername, loginUser} from "../../redux/reducers/user-reducer";

const mapStateToProps = (state: Store) => {
    return {
        loginCase: state.users.loginCase,
        checked: state.users.checked
    }
}

const Login: React.FC<any> = (props) => {
    return (
        <div>
            <Log 
                loginCase={props.loginCase}
                checked={props.checked}
                regUser={false}
                saveEmail={props.saveEmail}
                savePassword={props.savePassword}
                rememberUsername={props.rememberUsername}
                loginUser={props.loginUser}
            />
        </div>
    )
}

export default connect(mapStateToProps, { savePassword, saveEmail, rememberUsername, loginUser })(Login)