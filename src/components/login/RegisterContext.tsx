import React from "react";
import Log from "./Log";
import {connect} from "react-redux";
import type {Store} from "../../redux/store"
import {savePassword, validatePassword, saveEmail, saveUsername, rememberUsername, logUsers} from "../../redux/reducers/user-reducer";

const mapStateToProps = (state: Store) => {
    return {
        registerCase: state.users.registerCase,
        checked: state.users.checked
    }
}

const Register: React.FC<any> = (props) => {
    return (
        <div>
            <Log 
                registerCase={props.registerCase} 
                checked={props.checked}
                regUser={true} 
                saveEmail={props.saveEmail}
                savePassword={props.savePassword}
                validatePassword={props.validatePassword}
                saveUsername={props.saveUsername}
                rememberUsername={props.rememberUsername}
                logUsers={props.logUsers}
            />
        </div>
    )
}

export default connect(mapStateToProps, 
    { savePassword, validatePassword, saveEmail, saveUsername, rememberUsername, logUsers })
    (Register)