import React from "react";
import { NavLink } from "react-router-dom";
import { Form, Button, Row, Col, Stack, FloatingLabel, Alert } from "react-bootstrap";
import * as axios from "axios";
import s from "./style.module.css"

const Landing: React.FC<any> = (props) => {

    // React.useEffect(() => {

    // }, [])

    const [code, codeHandler] = React.useState("");
    const [input, inputHandler] = React.useState("");
    const [output, setOutput] = React.useState("");

    const runCode = (e: any) => {
        console.log(code)
        fetch('http://localhost:5001/algorithm-challenge/us-central1/runCode', {
            withCredentials: true,
            headers: {
                'Authorization': 'Token 2a441d77-bd28-4551-a7c0-0120a076cab5',
                'Content-type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({
                "stdin": input,
                "files": [
                    {
                        "name": "main.py",
                        "content": "" + code + ""
                    }
                ]
            })
        } as any)
            .then(function (data) {
                // debugger
                return data.json()

            })
            .then((data: any) => {
                setOutput(data.stdout)
                console.log(data)
            })
            .catch(err => {
                console.log(err);
            })
        e.preventDefault();
    }

    const codeValueHandler = (value: string, isCodeInput: boolean) => {
        isCodeInput ? codeHandler(value) : inputHandler(value)
    }

    const handleCodeKeyPress = (event: any) => {
        if (event.key === 'Enter') {
            codeHandler(() => `${code} \n`)
        }
    }

    const handleInputKeyPress = (event: any) => {
        if (event.key === 'Enter') {
            inputHandler(() => `${code} \n`)
        }
    }

    return (
        <>
            <Row>
                <Form>
                    <Col>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Write code here</Form.Label>
                            <Form.Control
                                as="textarea"
                                rows={6}
                                onChange={(e) => codeValueHandler(e.target.value, true)}
                                onKeyPress={handleCodeKeyPress}
                            />
                        </Form.Group>
                        <div className={s.middelware}>
                            <Button variant="primary" type="submit" onClick={(e) => runCode(e)}>
                                Run
                            </Button>
                        </div>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Enter input parameters here</Form.Label>
                            <Stack direction="horizontal" gap={3}>
                                <Form.Control
                                    as="textarea"
                                    rows={2}
                                    onChange={(e) => codeValueHandler(e.target.value, false)}
                                    onKeyPress={handleInputKeyPress}
                                />
                                <div className={s.middelware}>Output: {output}</div>
                            </Stack>
                        </Form.Group>
                    </Col>
                </Form>
            </Row>



        </>
    )
}

export default Landing