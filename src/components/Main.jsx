import React from 'react';
import Register from "./login/RegisterContext"
import Login from './login/LoginContext';
import Logout from "./login/Logout";
import {Route, Switch,Redirect} from 'react-router-dom';
import Landing from "./landing/Landing";
import Header from "./bar/Header";
import FinishSignUp from "./login/FinishSignUp"

import {checkUserAuth} from "../redux/reducers/user-reducer"
import {connect} from "react-redux";
import WithUserAuth  from '../hoc/WithUserAuth';
// import type {Store} from "../redux/store"

const mapStateToProps = (state) => {
    return {
        isUserAuth: state.users.isUserAuth,
        isAuth: state.users.isAuth
    }
}

const Main = (props) => {
    React.useEffect(() => {
        props.checkUserAuth()
    }, [props.isAuth])
    return (
        <div>
            <Header isAuth={props.isAuth} isUserAuth={props.isUserAuth} />
            {/* {!props.isAuth && <Redirect to="/user/login" />} */}
            {/* <Switch> */}
                <Route exact path="/" render={() => <Landing />} />
                <Route exact path="/user/register" render={() => <Register />} />
                <Route exact path="/user/login" render={() => <Login />} />
                <Route exact path="/user/logout" render={() => <Logout />} />
                <Route path="/user/finishSignUp" render={() => <FinishSignUp />} />
            {/* </Switch> */}
        </div>
    )
}

export default connect(mapStateToProps, {checkUserAuth})(Main)