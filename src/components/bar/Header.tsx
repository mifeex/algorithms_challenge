import React from "react";
import { NavLink } from "react-router-dom";
import { Navbar, Container } from "react-bootstrap";

const Header: React.FC<any> = (props) => {
    return (
        <Navbar>
            <Container>
                <Navbar.Brand><NavLink to="/">Algorithms Challenge</NavLink></Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                {props.isUserAuth.isAuth ?
                <>
                    <Navbar.Text>
                        Signed in as: <NavLink to={`/user/profile/id_${props.userID}`}>{props.isUserAuth.email}</NavLink>
                        /
                    </Navbar.Text>
                    <Navbar.Text>
                        <NavLink to="/user/logout">Logout</NavLink>
                    </Navbar.Text>
                </> :
                <>
                    <Navbar.Text>
                        <NavLink to={`/user/login`}>Login</NavLink>
                        /
                    </Navbar.Text>
                    <Navbar.Text>
                        <NavLink to="/user/register">Register</NavLink>
                    </Navbar.Text>
                </>
                }
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default Header