import './App.css';
import Main from './components/Main.jsx'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from "react-bootstrap";
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux'
import React from 'react';
import store from "./redux/store"

// function App(props) {
//   return (

//           <Main />

//   )
// }

class App extends React.Component {
  render() {
    return (
      <Router>
        <Provider store={store}>
          <Container>
            <Row>
              <Col>
                <Main />
              </Col>
            </Row>
          </Container>
        </Provider>
      </Router>
    )
  }
}

export default App;
