import db, { auth } from "../instance";
import { collection, getDocs, setDoc, addDoc, doc } from 'firebase/firestore/lite';
import Base64 from 'crypto-js/enc-base64';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import type { regUser, LoginUser, userInfo } from "../../../redux/reducers/user-reducer";
import { sendSignInLinkToEmail, createUserWithEmailAndPassword, signInWithEmailLink, onAuthStateChanged,  } from "firebase/auth";

export interface userAuthType {
    email: string | null;
    uid: string | null;
    isAuth: boolean;
}

// NOT WORKING COMONENT. SHOULD BE REMOVED

const data = {} as regUser;
const user = data.name;
export const userAuth = {
    email: "",
    uid: "",
    isAuth: false
} as userAuthType

// Add a new document with a generated id.

export async function addNewUserToDB(data: regUser) {
    const password = data.password

    createUserWithEmailAndPassword(auth, data.email, data.password) // create credentials for new user
    .then((userCredential) => {
        // Signed in 
        // debugger
        const user = userCredential.user;
        console.log(user)
        // ...
    })
    .catch((error) => {
        const errorCode = error.code;
        console.log(error.message)
        // ..
    });

    const docRef = await addDoc(collection(db, "users"), {
        ...data,
        password
    });

    console.log("Document written with ID: ", docRef.id);
}

// export const createNewUser = (data: regUser) => {
//     data = data;



// }

export const completeSighIn = (email: string | any, password: string | any) => {
    signInWithEmailLink(auth, email, window.location.href) // email verified
    .then((result) => {
        // Clear email from storage.
        window.localStorage.removeItem('emailForSignIn');
        // window.localStorage.removeItem('tokenForSignIn')
    })
    .catch((error) => {
        console.log(error.message)
    });

    // })
    // .catch((error) => {
    //     console.log(error.message)
    // });
}
// const cityRef = doc(db, 'email', 'password', "userName", "isVIP");
// setDoc(doc(db, 'email', 'password', "userName", "isVIP"), data);

// export const isUserAuth = () => ;


// export function loginUser(user: userInfo) {

// }



export async function getAllUsers() {
    let users = collection(db, "users")
    let usersCollection = await getDocs(users)
    usersCollection.docs.map((docs: any) => {
        console.log(docs.data());
    })
}

